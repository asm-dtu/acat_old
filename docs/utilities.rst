Other utilities
===============

.. automodule:: acat.utilities
   :members:
   :undoc-members:
   :show-inheritance:
   :exclude-members: expand_cell, is_list_or_tuple
